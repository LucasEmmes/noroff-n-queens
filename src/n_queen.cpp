#include <iostream>
#include <vector>
#include <stdlib.h>
#include <numeric>

void print_board(std::vector<std::vector<int>> &board) {
    int size = board.size();
    
    // Top line
    std::cout << "┌";
    for (int i = 0; i < size-1; i++) std::cout << "───┬";
    std::cout << "───┐" << std::endl;

    // Line with numbers, and the divider between
    for (int i = 0; i < size-1; i++)
    {
        std::cout << "│";
        for (int j = 0; j < size; j++)
        {
            if (board[i][j]) {
                std::cout << " X │";
            } else {
                std::cout << "   │";
            }
        }
        std::cout << std::endl;
    
        std::cout << "├";
        for (int j = 0; j < size-1; j++)
        {
            std::cout << "───┼";
        }
        std::cout << "───┤" << std::endl;
    }

    // Last number line
    std::cout << "│";
    for (int j = 0; j < size; j++)
    {
        if (board[size-1][j]) {
            std::cout << " X │";
        } else {
            std::cout << "   │";
        }
    }
    std::cout << std::endl;

    // Floor line
    std::cout << "└";
    for (int i = 0; i < size-1; i++)
    {
        std::cout << "───┴";
    }
    std::cout << "───┘" << std::endl;   
}

bool check_safe(const std::vector<std::pair<int, int>> &queens, const std::pair<int, int> &last) {
    for (std::pair<int, int> q : queens) {
        if (abs(q.first - last.first) == abs(q.second - last.second)) {
            return false;
        }
    }
    return true;
}

std::pair<int, int> latest;

bool solve(std::vector<std::pair<int, int>> &queens, std::vector<int> &validcols, std::vector<int> &validrows) {
    if (validrows.size() == 0) {
        return true;
    }

    int col;
    int y = validrows[validrows.size()-1];
    validrows.pop_back();

    for (int i = 0; i < validcols.size(); i++)
    {
        col = validcols[i];
        latest = std::pair<int, int>(y, col);

        if (check_safe(queens, latest)) { // Check for conflict

            validcols.erase(validcols.begin()+i);   // Remove column from valid list
            queens.push_back(latest);
            
            if (solve(queens, validcols, validrows)) {
                return true;                                // Pass return upwards if everything went well
            } else {
            
                validcols.insert(validcols.begin()+i, col); // Add back column
                queens.pop_back();
            
            }
        }
    }
    validrows.push_back(y);
    return false;
}


int main(int argc, char const *argv[])
{
    // int n = 8;
    int n = std::stoi(argv[1]);
    std::vector<std::pair<int, int>> queens;
    std::vector<int> validcols;
    validcols.reserve(n);
    int m = n/2;
    validcols.push_back(m);
    for (int i = 1; i <= m; i++)
    {
        if (m+i < n) {
            validcols.push_back(m+i);
        }
        validcols.push_back(m-i);
    }
    
    std::vector<int> validrows = validcols;

    solve(queens, validcols, validrows);

    return 0;
}
