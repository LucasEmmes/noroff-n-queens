#include <iostream>
#include <thread>
#include <vector>
#include <numeric>

bool stop = false;
// std::mutex gLock;

void print_board(std::vector<std::pair<int, int>> &queens) {
    // std::cout << "Found one in " << std::this_thread::get_id() << std::endl;
    int n = queens.size();

    std::vector<std::vector<int>> board(n, std::vector<int>(n, 0));

    // std::cout << queens.size() << std::endl;

    for (std::pair<int, int> q : queens)
        board[q.first][q.second] = 1;

    for (std::vector<int> row : board) {
        for (int i : row) {
            std::cout << i;
        }
        std::cout << std::endl;
    }


    // std::string output;
    // std::vector<std::vector<int>> board(n, std::vector<int>(n, 0));

    // for (std::pair<int, int> q : queens) {
    //     int y = q.first;
    //     int x = q.second;
    //     board[y][x] = 1;
    // }

    // for (std::vector<int> row : board) {
    //     for (int i : row) {
    //         output += (char) (i+48);
    //     }
    //     output += "\n";
    // }
    // std::cout << output << std::endl;
}

bool check_safe(const std::vector<std::pair<int, int>> &queens, const std::pair<int, int> &last) {
    for (std::pair<int, int> q : queens) {
        if (abs(q.first - last.first) == abs(q.second - last.second)) {
            return false;
        }
    }
    return true;
}


bool solve(int depth, std::vector<std::pair<int, int>> &queens, std::vector<int> &valid_rows, std::vector<int> &valid_cols) {
    
    // Base case
    if (depth < 0 || stop) {
        if (!stop) {
            stop = true;
            std::cout << "SOLUTION:" << std::endl;
            print_board(queens);
        }
        return true;
    }
    
    // Set up variables for this iteration;
    int col;
    std::pair<int, int> latest;
    int y = valid_rows[depth];

    // Iterate over all columns that are still valid
    for (int i = 0; i < valid_cols.size(); i++) {

        // Get next col and temporary queen position within that col
        col = valid_cols[i];
        latest = std::pair<int, int>(y, col);

        // Check if safe
        if (check_safe(queens, latest)) {
            // Is safe
            valid_cols.erase(valid_cols.begin()+i); // Remove column from valid list
            queens.push_back(latest);               // Add down newest queen
            
            // Check if works down the line
            if (solve(depth-1, queens, valid_rows, valid_cols)) {
                // Worked, return true
                return true;
            } else {
                // Didn't work
                valid_cols.insert(valid_cols.begin()+i, col);   // Place back the column
                queens.pop_back();  // Remove latest queen
            }
        }
    }
    valid_rows.push_back(y);
    return false;
}

void thread_start(int n, int num_of_threads, int thread_number) {
    // Set up queens
    std::vector<std::pair<int, int>> queens;
    // Set up rows (split amongst them)
    std::vector<int> valid_rows(n, 0);
    std::iota(valid_rows.begin(), valid_rows.end(), 0);
    for (int i = 0; i < n; i++) {
        std::swap(valid_rows[i], valid_rows[rand() % n]);
    }
    // Set up columns (alternating, not split)
    std::vector<int> valid_cols(n, 0);
    std::iota(valid_cols.begin(), valid_cols.end(), 0);
    for (int i = 0; i < n; i++) {
        std::swap(valid_cols[i], valid_cols[rand() % n]);
    }


    solve(n-1, queens, valid_rows, valid_cols);
}


int main(int argc, char const *argv[])
{
    int n = std::stoi(argv[1]);
    int t = std::stoi(argv[2]);
    // int n = 30;
    // int t = 2;
    
    // Rev up those threads

    // thread_start(n, t, 0);


    std::vector<std::jthread> jthreads;
    for (int i = 0; i < t; i++){
        jthreads.push_back(std::jthread(thread_start, n, t, i));
    }
    

    return 0;
}
