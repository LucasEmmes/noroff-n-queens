## N-queen problem solver

---  
### Background
The N-queen problem is based on the queen from the game chess.  
A queen can move anywhere along the same row, column, or diagonal she is in.  
The N-queen problem asks you to place N queens on an N*N sized chess board,  
such that no 2 queens can take each other out.

---
### How it works
This solver uses recursion to solve the n-queen problem.  
It starts by placing queens along the bottom row of the board, moving upwards with each recursive call.  
Whenever it places a queen, it checks for conflicts, moving along the column if there are any.  
It reverts back to a previous row if it runs out of options before solving the board.  

---
### Optimizations
1. **Only check last placed queen**:  
Whenever we place a new queen, we already know that the others are well placed with respect to each other,  
meaning we don't need to check the entire board, but only the squares accessible by the last placed queen.
2. **Only check diagonals**:  
Since we move up one row at a time, we need not check the current row for other queens, as there can't be any.  
I also implemented a vector which keeps track of available columns, removing them one at a time as they get queens placed in them.  
Because of this, we need not worry about the columns either.  
3. **Only check downwards**:  
Since we are moving upwards, there cannot be any queend above the one we just placed. As such we can omit checking upwards diagonals, and are only left with down-leftwards, and down-rightwards.
4. **Multithreading**:  
By having multiple threads working from different starting positions, the chance of reaching the goal early is increased, making it overall faster.

---
### How to use  
1. **For single-threaded backtracking**:  
To compile the program, do:  
`$ g++ n_queen.cpp -O3 -o queen.out`  
and then to use it do  
`./queen.out N`  
where N is the desired number of squares (should be fine for up to 30 squares)  
2. **For multithreaded backtracking**:
To compile the program, do:  
`$ g++ -std=c++20 n_queen_threaded.cpp -O3 -o queen.out`  
and then to use it do  
`./queen.out N T`  
where N is the desired number of squares and T is the desired number of threads.  
I have gotten this to work up to N=800.  
**NB**: If you have an N lower than around 20, there may be issues where multiple threads find a solution at exactly the same time.